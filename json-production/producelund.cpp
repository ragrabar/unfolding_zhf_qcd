#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>

#include "TFile.h"
#include "TTree.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/contrib/LundWithSecondary.hh"
#include "fastjet/contrib/LundJSON.hh"

using namespace std;
using namespace std::filesystem;
using namespace fastjet;
using namespace fastjet::contrib;

const double epsilon = 0.00001;
const double truth_match_angular_distance = 0.3;
LundGenerator lund;

PseudoJet pseudojet_from_ptphietam(double pt, double phi, double eta, double m) {
    return PseudoJet(sqrt(pt*pt*cosh(eta)*cosh(eta) + m*m), pt*cos(phi), pt*sin(phi), pt*sinh(eta));
}

vector<LundDeclustering> get_declusts_from_tree_vars(vector<double>* E, vector<double>* px,
                            vector<double>* py, vector<double>* pz, bool is_truth=false) {
    JetDefinition jet_def(antikt_algorithm, 1000);
    vector<PseudoJet> jet;
    int starter = 0;
    int step = 1;   
    if (is_truth) {
        // Check the condition for the first two elements
        // truth tracks are a bit fucked in that they are usually copied twice next to each other,
        // or sometimes there is a single useless particle at the beginning and then pairs.
        if (abs(px->at(0) - px->at(1)) < epsilon) {
            // Take every second element
           step = 2;
        } else if (abs(px->at(1) - px->at(2)) < epsilon) {
            // Take away the first element and then take every second element
           starter = 1;
           step = 2;
        }
    }

    for (int ittrack = starter; ittrack < E->size(); ittrack += step){
            // make the 0.9 GeV track pt cut
            if (sqrt(px->at(ittrack)*px->at(ittrack) + py->at(ittrack)*py->at(ittrack)) < 0.9) continue;
            PseudoJet track(px->at(ittrack), py->at(ittrack), pz->at(ittrack), E->at(ittrack));
            jet.push_back(track);
    }
    if (jet.size() == 0) {
        vector<LundDeclustering> empty;
        return empty;
    }
    ClusterSequence cs(jet, jet_def);
    PseudoJet reclust_jet = sorted_by_pt(cs.inclusive_jets(0))[0];
    vector<LundDeclustering> declusts = lund(reclust_jet);
    return declusts;
}	      

void fill_jsons(ofstream & b, ofstream & light, string source, string treename, bool is_data) {
    TFile *file = TFile::Open(source.c_str());
    // Check if the file was opened successfully
    if (!file || file->IsZombie()) {
        cerr << "Error: Unable to open file." << endl;
        return;
    }
    // Get a pointer to the TTree. ->Get gives something that the
    // compiler treats as a generic pointer to a TObject, but T(sth) is a
    // derived class from TObject -> to call tree a TTree we require type casting
    // Hence = (TTree*)file->Get("TrackJSSTrees_JSS");
    TTree *tree = (TTree*)file->Get(treename.c_str());  
    // Check if the tree exists
    if (!tree) {
        cerr << "Error: Tree not found." << endl;
        file->Close();
        return;
    }
    // Reco tracks (already with a pt > 0.9 GeV cut)
    vector<double>* energies0 = nullptr; 
    vector<double>* pxs0 = nullptr; 
    vector<double>* pys0 = nullptr;
    vector<double>* pzs0 = nullptr;
    vector<double>* energies1 = nullptr;
    vector<double>* pxs1 = nullptr;
    vector<double>* pys1 = nullptr;
    vector<double>* pzs1 = nullptr;
    vector<double>* energies2 = nullptr; 
    vector<double>* pxs2 = nullptr; 
    vector<double>* pys2 = nullptr; 
    vector<double>* pzs2 = nullptr;
    vector<double>* energies3 = nullptr;
    vector<double>* pxs3 = nullptr; 
    vector<double>* pys3 = nullptr;
    vector<double>* pzs3 = nullptr;
    // Truth charged particles (*without* a pt > 0.9 GeV cut, it is made in this code):
    vector<double>* tenergies0 = nullptr; 
    vector<double>* tpxs0 = nullptr; 
    vector<double>* tpys0 = nullptr;
    vector<double>* tpzs0 = nullptr;
    vector<double>* tenergies1 = nullptr;
    vector<double>* tpxs1 = nullptr;
    vector<double>* tpys1 = nullptr;
    vector<double>* tpzs1 = nullptr;
    vector<double>* tenergies2 = nullptr; 
    vector<double>* tpxs2 = nullptr; 
    vector<double>* tpys2 = nullptr; 
    vector<double>* tpzs2 = nullptr;
    vector<double>* tenergies3 = nullptr;
    vector<double>* tpxs3 = nullptr; 
    vector<double>* tpys3 = nullptr;
    vector<double>* tpzs3 = nullptr;
    double reco_w,
    DL1r0, DL1r1, DL1r2, DL1r3,
    jet0_eta, jet0_pt, jet0_m, jet0_phi,
    jet1_eta, jet1_pt, jet1_m, jet1_phi,
    jet2_eta, jet2_pt, jet2_m, jet2_phi,
    jet3_eta, jet3_pt, jet3_m, jet3_phi;
    double truth_w, 
    tflav0, tflav1, tflav2, tflav3,
    tjet0_eta, tjet0_pt, tjet0_m, tjet0_phi, 
    tjet1_eta, tjet1_pt, tjet1_m, tjet1_phi,
    tjet2_eta, tjet2_pt, tjet2_m, tjet2_phi, 
    tjet3_eta, tjet3_pt, tjet3_m, tjet3_phi;
    //// Set branch addresses to link variables with tree branches
    //Reco:
    tree->SetBranchAddress("jet0_matched_tracks_E", &energies0);
    tree->SetBranchAddress("jet0_matched_tracks_px", &pxs0);
    tree->SetBranchAddress("jet0_matched_tracks_py", &pys0);
    tree->SetBranchAddress("jet0_matched_tracks_pz", &pzs0);
    tree->SetBranchAddress("jet1_matched_tracks_E", &energies1);
    tree->SetBranchAddress("jet1_matched_tracks_px", &pxs1);
    tree->SetBranchAddress("jet1_matched_tracks_py", &pys1);
    tree->SetBranchAddress("jet1_matched_tracks_pz", &pzs1);
    tree->SetBranchAddress("jet2_matched_tracks_E", &energies2);
    tree->SetBranchAddress("jet2_matched_tracks_px", &pxs2);
    tree->SetBranchAddress("jet2_matched_tracks_py", &pys2);
    tree->SetBranchAddress("jet2_matched_tracks_pz", &pzs2);
    tree->SetBranchAddress("jet3_matched_tracks_E", &energies3);
    tree->SetBranchAddress("jet3_matched_tracks_px", &pxs3);
    tree->SetBranchAddress("jet3_matched_tracks_py", &pys3);
    tree->SetBranchAddress("jet3_matched_tracks_pz", &pzs3);
    tree->SetBranchAddress("jet0_eta",  &jet0_eta);
    tree->SetBranchAddress("jet0_pt", &jet0_pt);
    tree->SetBranchAddress("jet0_m", &jet0_m);
    tree->SetBranchAddress("jet0_phi", &jet0_phi);
    tree->SetBranchAddress("jet1_eta",  &jet1_eta);
    tree->SetBranchAddress("jet1_pt", &jet1_pt);
    tree->SetBranchAddress("jet1_m", &jet1_m);
    tree->SetBranchAddress("jet1_phi", &jet1_phi);
    tree->SetBranchAddress("jet2_eta",  &jet2_eta);
    tree->SetBranchAddress("jet2_pt", &jet2_pt);
    tree->SetBranchAddress("jet2_m", &jet2_m);
    tree->SetBranchAddress("jet2_phi", &jet2_phi);
    tree->SetBranchAddress("jet3_eta",  &jet3_eta);
    tree->SetBranchAddress("jet3_pt", &jet3_pt);
    tree->SetBranchAddress("jet3_m", &jet3_m);
    tree->SetBranchAddress("jet3_phi", &jet3_phi);
    tree->SetBranchAddress("jet0_DL1r", &DL1r0);
    tree->SetBranchAddress("jet1_DL1r", &DL1r1);
    tree->SetBranchAddress("jet2_DL1r", &DL1r2);
    tree->SetBranchAddress("jet3_DL1r", &DL1r3);
    tree->SetBranchAddress("reco_w", &reco_w);
    if (!is_data) {
        tree->SetBranchAddress("tjet0_matched_tracks_E", &tenergies0);
        tree->SetBranchAddress("tjet0_matched_tracks_px", &tpxs0);
        tree->SetBranchAddress("tjet0_matched_tracks_py", &tpys0);
        tree->SetBranchAddress("tjet0_matched_tracks_pz", &tpzs0);
        tree->SetBranchAddress("tjet1_matched_tracks_E", &tenergies1);
        tree->SetBranchAddress("tjet1_matched_tracks_px", &tpxs1);
        tree->SetBranchAddress("tjet1_matched_tracks_py", &tpys1);
        tree->SetBranchAddress("tjet1_matched_tracks_pz", &tpzs1);
        tree->SetBranchAddress("tjet2_matched_tracks_E", &tenergies2);
        tree->SetBranchAddress("tjet2_matched_tracks_px", &tpxs2);
        tree->SetBranchAddress("tjet2_matched_tracks_py", &tpys2);
        tree->SetBranchAddress("tjet2_matched_tracks_pz", &tpzs2);
        tree->SetBranchAddress("tjet3_matched_tracks_E", &tenergies3);
        tree->SetBranchAddress("tjet3_matched_tracks_px", &tpxs3);
        tree->SetBranchAddress("tjet3_matched_tracks_py", &tpys3);
        tree->SetBranchAddress("tjet3_matched_tracks_pz", &tpzs3);
        tree->SetBranchAddress("tjet0_flav", &tflav0); 
        tree->SetBranchAddress("tjet1_flav", &tflav1); 
        tree->SetBranchAddress("tjet2_flav", &tflav2); 
        tree->SetBranchAddress("tjet3_flav", &tflav3); 
        tree->SetBranchAddress("truth_w", &truth_w);
        tree->SetBranchAddress("tjet0_eta",  &tjet0_eta);
        tree->SetBranchAddress("tjet0_pt", &tjet0_pt);
        tree->SetBranchAddress("tjet0_m", &tjet0_m);
        tree->SetBranchAddress("tjet0_phi", &tjet0_phi);
        tree->SetBranchAddress("tjet1_eta",&tjet1_eta);
        tree->SetBranchAddress("tjet1_pt", &tjet1_pt);
        tree->SetBranchAddress("tjet1_m",  &tjet1_m);
        tree->SetBranchAddress("tjet1_phi",&tjet1_phi);
        tree->SetBranchAddress("tjet2_eta",&tjet2_eta);
        tree->SetBranchAddress("tjet2_pt", &tjet2_pt);
        tree->SetBranchAddress("tjet2_m",  &tjet2_m);
        tree->SetBranchAddress("tjet2_phi",&tjet2_phi);
        tree->SetBranchAddress("tjet3_eta",&tjet3_eta);
        tree->SetBranchAddress("tjet3_pt", &tjet3_pt);
        tree->SetBranchAddress("tjet3_m",  &tjet3_m);
        tree->SetBranchAddress("tjet3_phi",&tjet3_phi);
    }
    // Event loop
    int totalentries = int(tree->GetEntries());
    for (int iEntry = 0; iEntry < totalentries; iEntry++) {
        tree->GetEntry(iEntry);
        // Reco:
        vector<vector<double>*> energies{energies0, energies1, energies2, energies3},
        pxs{pxs0, pxs1, pxs2, pxs3}, pys{pys0, pys1, pys2, pys3}, pzs{pzs0, pzs1, pzs2, pzs3};
        vector<double> jetetas{jet0_eta, jet1_eta, jet2_eta, jet3_eta},
        jetms{jet0_m, jet1_m, jet2_m, jet3_m}, jetpts{jet0_pt, jet1_pt, jet2_pt, jet3_pt},
        jetphis{jet0_phi, jet1_phi, jet2_phi, jet3_phi},
        DL1rs{DL1r0, DL1r1, DL1r2, DL1r3};
        // Truth:
        vector<vector<double>*> tenergies{tenergies0, tenergies1, tenergies2, tenergies3},
        tpxs{tpxs0, tpxs1, tpxs2, tpxs3}, tpys{tpys0, tpys1, tpys2, tpys3}, tpzs{tpzs0, tpzs1, tpzs2, tpzs3};
        vector<double> tjetetas{tjet0_eta, tjet1_eta, tjet2_eta, tjet3_eta},
        tjetms{tjet0_m, tjet1_m, tjet2_m, tjet3_m}, tjetpts{tjet0_pt, tjet1_pt, tjet2_pt, tjet3_pt},
        tjetphis{tjet0_phi, tjet1_phi, tjet2_phi, tjet3_phi},
        tflavs{tflav0, tflav1, tflav2, tflav3}; //not useful for now, but we could try to check DL1R bias
        vector<PseudoJet> reco_pjs;
        for (int ireco = 0; ireco < 4; ireco++) {
            if (jetms[ireco] < 0 || energies[ireco]->size() < 3) continue;
            PseudoJet pjet = pseudojet_from_ptphietam(jetpts[ireco], jetphis[ireco], jetetas[ireco], jetms[ireco]);
            vector<LundDeclustering> reco_declusts = get_declusts_from_tree_vars(energies[ireco], 
                                                     pxs[ireco], pys[ireco], pzs[ireco]);
            int match_index = -1;
            vector<LundDeclustering> truth_declusts;
            if (!is_data) {
                double maxpt = 0;
                PseudoJet matched_truth_jet(0,0,0,0);
                for (int itruth = 0; itruth < 4; itruth++) {
                    PseudoJet truthpjet = pseudojet_from_ptphietam(tjetpts[itruth], tjetphis[itruth], tjetetas[itruth], tjetms[itruth]);
                    if (pjet.delta_R(truthpjet) < truth_match_angular_distance and truthpjet.pt() > matched_truth_jet.pt()) {
                        matched_truth_jet = truthpjet;
                        match_index = itruth;
                    }
                }
                if(match_index != -1 && tenergies[match_index]->size() > 2) truth_declusts = get_declusts_from_tree_vars(tenergies[match_index], tpxs[match_index], tpys[match_index], tpzs[match_index], true);   
            }
            // 70% working point 2.1950 3.2450
            ofstream* dump = (DL1rs[ireco] > 2.1950 ? &b : &light);
            *dump << ",\n{\n";
            *dump << "     \"weight\":" << reco_w << ",\n";
            *dump << "     \"pt\":"     << jetpts[ireco] << ",\n";
            *dump << "     \"eta\":"    << jetetas[ireco] << ",\n";
            *dump << "     \"mass\":"   << jetms[ireco] << ",\n";
            *dump << "     \"phi\":"    << jetphis[ireco] << ",\n";
            *dump << "     \"DL1r\":"   << DL1rs[ireco] << ",\n";
            *dump << "     \"emissions\":";
            lund_to_json(*dump, reco_declusts); 
            if (is_data) {
                    *dump << "\n";
            }
            else {
                    *dump << ",\n";
                if (match_index != -1 ){
                    *dump << "     \"matched truth\":" << 1 << ",\n";
                    *dump << "     \"tweight\":" << truth_w << ",\n";
                    *dump << "     \"tpt\":"     << tjetpts[match_index] << ",\n";
                    *dump << "     \"teta\":"    << tjetetas[match_index] << ",\n";
                    *dump << "     \"tmass\":"   << tjetms[match_index] << ",\n";
                    *dump << "     \"tphi\":"    << tjetphis[match_index] << ",\n";
                    *dump << "     \"temissions\":";
                    lund_to_json(*dump, truth_declusts); *dump << "\n";
                } else {
                    *dump << "     \"matched truth\":" << 0 << "\n";
                }
            }
            *dump << "}";

        }
    } // event loop
    file->Close();
 // fill jsons
}
int main(int argc, char* argv[]) {
    if (argc < 2) {
        cerr << "Usage: " << argv[0] << " <directory_path>" << endl;
        return 1;
    }

    string bfile            = "bjets-MCsignal.json";
    string lightfile        = "lightjets-MCsignal.json";
    string bfiledata        = "bjets-data.json";
    string lightfiledata    = "lighjets-data.json";
    ofstream b(bfile.c_str());
    ofstream light(lightfile.c_str());
    ofstream bdata(bfiledata.c_str());
    ofstream lightdata(lightfiledata.c_str());   
    double counterb=0 , counterlight=0, counterbdata=0, counterlightdata=0;
    string directory_path = argv[1];
    vector<string> required_subdirs = {"mc16a", "mc16d", "mc16e"};
    b << "[\n{}";
    light << "[\n{}";
    bdata << "[\n{}";
    lightdata << "[\n{}";
    for (const auto& subdir : required_subdirs) {
        path fullPath = path(directory_path) / subdir;
        if (exists(fullPath) && is_directory(fullPath)) {
            // Iterate over the contents of the subdirectory
            for (const auto& entry : directory_iterator(fullPath)) {
                if (!entry.is_regular_file() || !(entry.path().extension() == ".root")) continue;
                if (entry.path().filename().string().find("data") != string::npos) {
                    cout << "Processing " << entry.path() << endl; 
                    fill_jsons(bdata, lightdata, entry.path(), "data_Mu_Geq2BiJ_AllM_selTree", true);
                    fill_jsons(bdata, lightdata, entry.path(), "data_El_Geq2BiJ_AllM_selTree", true);  
                    cout << "Hell yeah, it worked" << endl;
                }  else {
                    if (entry.path().filename().string().find("mumu") != string::npos) {
                        cout << "Processing " << entry.path() << endl; 
                        fill_jsons(b, light, entry.path(), "ZFxFx_NB_Mu_Geq2BiJ_AllM_selTree", false);
                        cout << "Hell yeah, it worked" << endl;
                    } else if (entry.path().filename().string().find("ee") != string::npos) {
                        cout << "Processing " << entry.path() << endl; 
                        fill_jsons(b, light, entry.path(), "ZFxFx_NB_El_Geq2BiJ_AllM_selTree", false); 
                        cout << "Hell yeah, it worked" << endl;
                    } else {
                        cout << "Skipping " << entry.path() << endl;
                    }
                }   


                
            }
        }
    }

    b << "\n]";
    light << "\n]";
    bdata << "\n]";
    lightdata << "\n]";

    // Close output files.
    b.close();
    light.close();
    bdata.close();
    lightdata.close();
    return 0;
}
