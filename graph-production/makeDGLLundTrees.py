#!/usr/bin/env python3
import ROOT as root 
import fastjet as fj
import numpy as np 
import networkx as nx
import matplotlib.pyplot as plt
import scipy
import pydot
from networkx.drawing.nx_pydot import graphviz_layout
import argparse
import dgl
from dgl.data.utils import save_graphs

def LundDeclustering(jharder, jsofter):
    # for two PseudoJets, calculate Lund Coordinates.
    # the order and definitions should be the same as in LundNet source code
    lnz = np.float32(np.log(jsofter.pt()/(jharder.pt() + jsofter.pt())))
    delta = jharder.delta_R(jsofter)
    if (delta == 0):
        return [0,0,0,0,0]
    lndelta = np.double(np.log(jharder.delta_R(jsofter)))
    lnkt = np.float32(np.log(jsofter.pt() * delta))
    lnm = np.float32(0.5 * np.log(abs((jharder + jsofter).m2())))
    try:
        psi = np.float32(np.arctan2((jharder.rap() - jsofter.rap()) , (jharder.phi() - jsofter.phi())))
    except ZeroDivisionError:
        psi = 0

    return [lnz, lndelta, psi, lnm, lnkt]

def main():
    parser = argparse.ArgumentParser(description='Parser for input file')
    parser.add_argument('ntuplepath', type=str, help='Path to the directory containing hist- files (must be from the JetPhoton SM group eos disk for JSS)')
    parser.add_argument('-nev', '--nevents', type=int, default=2, help='Number of events to process (will take full file if this number exceeds the total number of events)')
    parser.add_argument('-verbose', help= 'Verbose mode prints number of jets processed each 100 of jets', action="store_true")
    args = parser.parse_args()

    inFileName = args.ntuplepath #"../user.csauer.27842791._000055.ANALYSIS.root"
    inFile = root.TFile.Open(inFileName ,"READ")
    tree = inFile.Get("ZFxFx_El_Geq2BiJ_selTree")
    totalentries = min(tree.GetEntries(), args.nevents)
    counter = 0
    LundTreeRecoCollection = []
    LundTreeTruthCollection = []
    for entryNum in range(0,totalentries):

        tree.GetEntry(entryNum)
        reco_w =  getattr(tree,     "reco_w")
        jet0DL1R = getattr(tree,    "jet0_DL1r")
        jet1DL1R = getattr(tree,    "jet1_DL1r")
        jet2DL1R = getattr(tree,    "jet2_DL1r")
        jet3DL1R = getattr(tree,    "jet3_DL1r")
        jetDL1Rs = np.array([jet0DL1R, jet1DL1R, jet2DL1R, jet3DL1R])
        jet0eta = getattr(tree,    "jet0_eta")
        jet1eta = getattr(tree,    "jet1_eta")
        jet2eta = getattr(tree,    "jet2_eta")
        jet3eta = getattr(tree,    "jet3_eta")
        jetetas = np.array([jet0eta, jet1eta, jet2eta, jet3eta])
        jet0phi = getattr(tree,    "jet0_phi")
        jet1phi = getattr(tree,    "jet1_phi")
        jet2phi = getattr(tree,    "jet2_phi")
        jet3phi = getattr(tree,    "jet3_phi")
        jetphis = np.array([jet0phi, jet1phi, jet2phi, jet3phi])
        tjet0eta = getattr(tree,    "tjet0_eta")
        tjet1eta = getattr(tree,    "tjet1_eta")
        tjet2eta = getattr(tree,    "tjet2_eta")
        tjet3eta = getattr(tree,    "tjet3_eta")
        tjetetas = np.array([tjet0eta, tjet1eta, tjet2eta, tjet3eta])
        tjet0phi = getattr(tree,    "tjet0_phi")
        tjet1phi = getattr(tree,    "tjet1_phi")
        tjet2phi = getattr(tree,    "tjet2_phi")
        tjet3phi = getattr(tree,    "tjet3_phi")
        tjetphis = np.array([tjet0phi, tjet1phi, tjet2phi, tjet3phi])        
        tjet0pt = getattr(tree,    "tjet0_pt")
        tjet1pt = getattr(tree,    "tjet1_pt")
        tjet2pt = getattr(tree,    "tjet2_pt")
        tjet3pt = getattr(tree,    "tjet3_pt")
        tjetpts = np.array([tjet0pt, tjet1pt, tjet2pt, tjet3pt])

        energies0 =  getattr(tree,     "jet0_matched_tracks_E")
        pxs0 =       getattr(tree,     "jet0_matched_tracks_px")
        pys0 =       getattr(tree,     "jet0_matched_tracks_py")
        pzs0 =       getattr(tree,     "jet0_matched_tracks_pz")
        tenergies0 =  getattr(tree,     "tjet0_matched_tracks_E")
        tpxs0 =       getattr(tree,     "tjet0_matched_tracks_px")
        tpys0 =       getattr(tree,     "tjet0_matched_tracks_py")
        tpzs0 =       getattr(tree,     "tjet0_matched_tracks_pz")

        energies1 =  getattr(tree,     "jet1_matched_tracks_E")
        pxs1 =       getattr(tree,     "jet1_matched_tracks_px")
        pys1 =       getattr(tree,     "jet1_matched_tracks_py")
        pzs1 =       getattr(tree,     "jet1_matched_tracks_pz")
        tenergies1 =  getattr(tree,     "tjet1_matched_tracks_E")
        tpxs1 =       getattr(tree,     "tjet1_matched_tracks_px")
        tpys1 =       getattr(tree,     "tjet1_matched_tracks_py")
        tpzs1 =       getattr(tree,     "tjet1_matched_tracks_pz")

        energies2 =  getattr(tree,     "jet2_matched_tracks_E")
        pxs2 =       getattr(tree,     "jet2_matched_tracks_px")
        pys2 =       getattr(tree,     "jet2_matched_tracks_py")
        pzs2 =       getattr(tree,     "jet2_matched_tracks_pz")
        tenergies2 =  getattr(tree,     "tjet2_matched_tracks_E")
        tpxs2 =       getattr(tree,     "tjet2_matched_tracks_px")
        tpys2 =       getattr(tree,     "tjet2_matched_tracks_py")
        tpzs2 =       getattr(tree,     "tjet2_matched_tracks_pz")

        energies3 =  getattr(tree,     "jet3_matched_tracks_E")
        pxs3 =       getattr(tree,     "jet3_matched_tracks_px")
        pys3 =       getattr(tree,     "jet3_matched_tracks_py")
        pzs3 =       getattr(tree,     "jet3_matched_tracks_pz")
        tenergies3 =  getattr(tree,     "tjet3_matched_tracks_E")
        tpxs3 =       getattr(tree,     "tjet3_matched_tracks_px")
        tpys3 =       getattr(tree,     "tjet3_matched_tracks_py")
        tpzs3 =       getattr(tree,     "tjet3_matched_tracks_pz")

        tjet_flav0 = getattr(tree,      "tjet0_flav")
        tjet_flav1 = getattr(tree,      "tjet1_flav")
        tjet_flav2 = getattr(tree,      "tjet2_flav")
        tjet_flav3 = getattr(tree,      "tjet3_flav")

        tflavs = [tjet_flav0, tjet_flav1, tjet_flav2, tjet_flav3]

        jet_energies = [energies0, energies1, energies2, energies3]
        jet_pxs = [pxs0, pxs1, pxs2, pxs3]
        jet_pys = [pys0, pys1, pys2, pys3]
        jet_pzs = [pzs0, pzs1, pzs2, pzs3]

        tjet_energies = [tenergies0, tenergies1, tenergies2, tenergies3]
        tjet_pxs = [tpxs0, tpxs1, tpxs2, tpxs3]
        tjet_pys = [tpys0, tpys1, tpys2, tpys3]
        tjet_pzs = [tpzs0, tpzs1, tpzs2, tpzs3]
        #jet0m = getattr(tree,    "jet0_m")
        #jet1m = getattr(tree,    "jet1_m")
        #jet2m = getattr(tree,    "jet2_m")
        #jet3m = getattr(tree,    "jet3_m")
        #jetms = np.array([jet0m, jet1m, jet2m, jet3m])
        #jetpts = np.array([jet0pt, jet1pt, jet2pt, jet3pt])
        #lep0eta =  getattr(tree,    "lep0_eta")
        ##lep1eta =  getattr(tree,    "lep1_eta")
        ###lep0phi =  getattr(tree,    "lep0_phi")
        ###lep1phi =  getattr(tree,    "lep1_phi")
        ##lep0pt =  getattr(tree,    "lep0_pt")
        ##lep1pt =  getattr(tree,    "lep1_pt")
        #met = getattr(tree,        "met_pt")
        bcounter = 0
        tbcounter = 0
        for i in range(4): 
            # 77% working point of DL1R
            if jetDL1Rs[i] > 2.1950 and len(jet_energies[i]) > 2 and jet_energies[i][0] > 0.:
                #print("Making a new reco graph...")
                #print("length of pxs = ", len(jet_pxs[i]))
                jmax = -1
                ptmax = 0.
                for j in range(4):
                    if len(tjet_energies[j]) == 0: continue
                    if tjet_energies[j][0] < 0.: continue
                    if len(tjet_energies[j]) < 4: continue
                    dphi = tjetphis[j] - jetphis[i]
                    if dphi > np.pi: dphi = dphi - np.pi
                    if dphi < -np.pi: dphi = dphi + np.pi
                    if (dphi)**2 + (tjetetas[j] - jetetas[i])**2 > 0.3**2: continue
                    if tjetpts[j] > ptmax:
                        ptmax = tjetpts[j]
                        jmax = j
                if (jmax == -1): continue
                if(abs(tjet_pxs[jmax][0] - tjet_pxs[jmax][1]) < 0.0001):
                    tjet_energies[jmax]= tjet_energies[jmax][::2]
                    tjet_pxs[jmax]      = tjet_pxs[jmax][::2]
                    tjet_pys[jmax]      = tjet_pys[jmax][::2]
                    tjet_pzs[jmax]      = tjet_pzs[jmax][::2]
                elif(abs(tjet_pxs[jmax][1] - tjet_pxs[jmax][2]) < 0.0001): # THIS NEEDS A CHECK. some events have one particle and then duplicated ones...
                    tjet_energies[jmax]= tjet_energies[jmax][1::2]
                    tjet_pxs[jmax]      = tjet_pxs[jmax][1::2]
                    tjet_pys[jmax]      = tjet_pys[jmax][1::2]
                    tjet_pzs[jmax]      = tjet_pzs[jmax][1::2]
                elif(abs(tjet_pxs[jmax][2] - tjet_pxs[jmax][3]) < 0.0001): # THIS NEEDS A CHECK. some events have one particle and then duplicated ones...
                    tjet_energies[jmax]= tjet_energies[jmax][2::2]
                    tjet_pxs[jmax]      = tjet_pxs[jmax][2::2]
                    tjet_pys[jmax]      = tjet_pys[jmax][2::2]
                    tjet_pzs[jmax]      = tjet_pzs[jmax][2::2]
                if len(tjet_energies[jmax]) < 2: continue
                LundRecoTree = networkx_from_constitsEpxpypz(jet_energies[i], jet_pxs[i], jet_pys[i], jet_pzs[i],\
                           charges = None, follow_primary = False)
                DGLLundRecoTree = dgl.from_networkx(LundRecoTree, node_attrs=['features'])
                LundTreeRecoCollection.append(DGLLundRecoTree)
                LundTruthTree = networkx_from_constitsEpxpypz(tjet_energies[jmax], tjet_pxs[jmax], tjet_pys[jmax], tjet_pzs[jmax],\
                           charges = None, follow_primary = False)
                DGLLundTruthTree = dgl.from_networkx(LundTruthTree, node_attrs=['features'])
                LundTreeTruthCollection.append(DGLLundTruthTree)                
            #if tflavs[i] == 5.0 and len(tjet_energies[i]) > 2 and tjet_energies[i][0] > 0.:
            #    # BE CAREFUL! truth tracks are doubled (px1, px1, px2, px2, ...) and so on. Need to filter to adapt to that
            #    #print("Before trimming, pxs = ", tjet_pxs[i])
            #    LundTruthTree = networkx_from_constitsEpxpypz(tjet_energies[i], tjet_pxs[i], tjet_pys[i], tjet_pzs[i],\
            #                   charges = None, follow_primary = False)
            #    DGLLundTruthTree = dgl.from_networkx(LundTruthTree, node_attrs=['features'])
            #    LundTreeTruthCollection.append(DGLLundTruthTree)
        counter += 1
    dgl.save_graphs("./DGLLundRecoTreesZFxFx_Mu_" + str(counter)+ "jets.bin", LundTreeRecoCollection)
    dgl.save_graphs("./DGLLundTruthTreesZFxFx_Mu" + str(counter)+ "jets.bin", LundTreeTruthCollection)

def networkx_from_constitsEpxpypz(Es, pxs, pys, pzs, charges = None, follow_primary = False):
    jet_def = fj.JetDefinition(fj.cambridge_algorithm, 1000.0)

    if charges is not None:
        filter_array = lambda array_to_filter, charge_array: [x for x, c in zip(array_to_filter, charge_array) if c != 0]
        Es =   filter_array(Es, charges)
        pxs =  filter_array(pts, charges)
        pys = filter_array(etas, charges)
        pzs = filter_array(phis, charges)
    input_pjs = [fj.PseudoJet(pxs[i], pys[i], pzs[i], Es[i]) \
                 for i in range(len(Es))]
    # 22.08.2023: attempt at looking directly at the cs.jets() list
    # and creating a Lund Tree from it
    cs = fj.ClusterSequence(input_pjs, jet_def)
    jetlist = cs.jets()[len(input_pjs):] # the PseudoJets cut out here are just input_pjs
    jetlist = jetlist[::-1] # now the first element = full pseudojet
    if follow_primary:
        hardguys = [jetlist[0]]

    assert len(jetlist) == len(input_pjs) - 1 # if cs.jets() works correctly, must be the case
    LundTree = nx.Graph()
    #print("length of input pjs = ", len(input_pjs))
    for i_j in range(len(jetlist)):
        # check if the jet is a result of a recombination
        # the first n entries in jetlist are just input_pjs
        # so they should have been cut out by now anyway

        current_jet = jetlist[i_j]
        child = fj.PseudoJet()
        parharder = fj.PseudoJet()
        parsofter = fj.PseudoJet()
        if not current_jet.has_parents(parharder,parsofter): continue
        
        if parsofter.pt() > parharder.pt(): parharder, parsofter = parsofter, parharder

        LundCoord = LundDeclustering(parharder,parsofter)
        if LundCoord == [0,0,0,0,0]:
            print("---------------------------------------------------")
            print("Badly trimmed ! pxs = ", pxs)
        LundTree.add_node(i_j, features = LundCoord)

        if follow_primary:
            hardchild = fj.PseudoJet()
            if (not parharder.has_child(hardchild)) or (hardchild in hardguys):
                hardguys.append(parharder)
                LundTree.nodes[i_j]["follow_primary"] = "primary"
            else:
                LundTree.nodes[i_j]["follow_primary"] = "secondary"

        if current_jet.has_child(child):
            LundTree.add_edge(jetlist.index(child), i_j)

    return LundTree

def networkx_from_constitsEptetaphi(Es, pts, etas, phis, charges = None, follow_primary = False):
    jet_def = fj.JetDefinition(fj.cambridge_algorithm, 1000.0)

    if charges is not None:
        filter_array = lambda array_to_filter, charge_array: [x for x, c in zip(array_to_filter, charge_array) if c != 0]
        Es =   filter_array(Es, charges)
        pts =  filter_array(pts, charges)
        etas = filter_array(etas, charges)
        phis = filter_array(phis, charges)

    input_pjs = [fj.PseudoJet(pts[i]*np.cos(phis[i]), pts[i]*np.sin(phis[i]), pts[i]*np.sinh(etas[i]), Es[i]) \
                 for i in range(len(Es))]
    # 22.08.2023: attempt at looking directly at the cs.jets() list
    # and creating a Lund Tree from it
    cs = fj.ClusterSequence(input_pjs, jet_def)
    jetlist = cs.jets()[len(input_pjs):] # the PseudoJets cut out here are just input_pjs
    jetlist = jetlist[::-1] # now the first element = full pseudojet
    if follow_primary:
        hardguys = [jetlist[0]]

    assert len(jetlist) == len(input_pjs) - 1 # if cs.jets() works correctly, must be the case
    LundTree = nx.Graph()
    
    for i_j in range(len(jetlist)):
        # check if the jet is a result of a recombination
        # the first n entries in jetlist are just input_pjs
        # so they should have been cut out by now anyway

        current_jet = jetlist[i_j]
        child = fj.PseudoJet()
        parharder = fj.PseudoJet()
        parsofter = fj.PseudoJet()
        if not current_jet.has_parents(parharder,parsofter): continue
        
        if parsofter.pt() > parharder.pt(): parharder, parsofter = parsofter, parharder

        LundCoord = LundDeclustering(parharder,parsofter)
        LundTree.add_node(i_j, features = LundCoord)

        if follow_primary:
            hardchild = fj.PseudoJet()
            if (not parharder.has_child(hardchild)) or (hardchild in hardguys):
                hardguys.append(parharder)
                LundTree.nodes[i_j]["follow_primary"] = "primary"
            else:
                LundTree.nodes[i_j]["follow_primary"] = "secondary"

        if current_jet.has_child(child):
            LundTree.add_edge(jetlist.index(child), i_j)

    return LundTree

if __name__ == '__main__':
    root.gROOT.SetBatch()
    main()
